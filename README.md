# README #


### What is this repository for? ###

* This is a console application that allows user to create a form.
* The form contains validation for a specific age group and mantains person's and their spouse's details.
* This project also illustrate about writing unit test cases for various scenarios.

### How do I set up? ###

* Clone the solution with Visual studio installed in your stand-alone environment.
* Launch the solution
* Change app.config to provide the path in your environment to save person's details.

### Happy Coding