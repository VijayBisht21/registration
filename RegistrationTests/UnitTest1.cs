using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using Registration;
using System.IO;
using System;
namespace RegistrationTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestInitialize]
        public void SetUp()
        {
            ConfigurationManager.AppSettings["dateformat"] = "dd-MM-yyyy";
            ConfigurationManager.AppSettings["SpousePath"] = "/Users/vijaybisht/Projects/CompanyTask/Escher/UnitTest/Spouse";
            ConfigurationManager.AppSettings["PeoplePath"] = "/Users/vijaybisht/Projects/CompanyTask/Escher/UnitTest/People.txt";
        }
        [TestMethod]
        public void validateFirstName_withInvalidformat()
        {
            var reg = new RegistrationAttributes();
            var input = new StringReader("Vijay");
            Console.SetIn(input);
            reg.FirstName = "Vijay1";
            Assert.AreEqual("Vijay", reg.FirstName);
        }
        [TestMethod]
        public void validateSurName_withInvalidformat()
        {
            var reg = new RegistrationAttributes();
            var input = new StringReader("Bisht");
            Console.SetIn(input);
            reg.SurName = "Bisht1";
            Assert.AreEqual("Bisht", reg.SurName);
        }
        [TestMethod]
        public void validateDOB_withInvalidformat()
        {
            var reg = new RegistrationAttributes();
            var input = new StringReader("21-06-1990");
            Console.SetIn(input);
            reg.DOB = "21-jun-1990";
            Assert.AreEqual("21-06-1990", reg.DOB);
        }
        [TestMethod]
        public void ValidateAge_GreaterThan18()
        {
            var reg = new RegistrationAttributes();
            reg.DOB = "21-06-1990";
            Assert.AreEqual(true, RegistrationMain.ValidateAge(reg));
        }
        [TestMethod]
        public void ValidateAge_Between16To18_WithAcceptTerms()
        {
            var reg = new RegistrationAttributes();
            var input = new StringReader("Y");
            Console.SetIn(input);
            reg.DOB = "21-06-2003";
            Assert.AreEqual(true, RegistrationMain.ValidateAge(reg));
        }
        [TestMethod]
        public void ValidateAge_Between16To18_WithoutAcceptTerms()
        {
            var reg = new RegistrationAttributes();
            var input = new StringReader("N");
            Console.SetIn(input);
            reg.DOB = "21-06-2003";
            Assert.AreEqual(false, RegistrationMain.ValidateAge(reg));
        }
        [TestMethod]
        public void ValidateAge_LessThan16()
        {
            var reg = new RegistrationAttributes();
            reg.DOB = "21-06-2005";
            Assert.AreEqual(false, RegistrationMain.ValidateAge(reg));
        }
        [TestMethod]
        public void CheckMaritalStatus_Married()
        {
            var reg = new RegistrationAttributes();
            var inputs = string.Join(Environment.NewLine, new[] { "1", "wife", "lastname", "01-01-2000" });
            var input = new StringReader(inputs);
            Console.SetIn(input);
            Assert.AreEqual(true, RegistrationMain.CheckMaritalStatus(reg));
        }
        [TestMethod]
        public void CheckMaritalStatus_Single()
        {
            var reg = new RegistrationAttributes();
            var input = new StringReader("2");
            Console.SetIn(input);
            Assert.AreEqual(true, RegistrationMain.CheckMaritalStatus(reg));
        }
        [TestMethod]
        public void CheckMaritalStatus_NotSingleNotMarried_WithNoRetry()
        {
            var reg = new RegistrationAttributes();
            var inputs = string.Join(Environment.NewLine, new[] { "3", "N" });
            var input = new StringReader(inputs);
            Console.SetIn(input);
            Assert.AreEqual(false, RegistrationMain.CheckMaritalStatus(reg));
        }
        [TestMethod]
        public void CheckMaritalStatus_NotSingleNotMarried_WithRetry()
        {
            var reg = new RegistrationAttributes();
            var inputs = string.Join(Environment.NewLine, new[] { "3", "Y", "2" });
            var input = new StringReader(inputs);
            Console.SetIn(input);
            Assert.AreEqual(true, RegistrationMain.CheckMaritalStatus(reg));
        }
        [TestMethod]
        public void StoreInfo_CreateDirectoryWithFile()
        {
            string content = "Vijay|Bisht|21-06-1990|Married|/Users/vijaybisht/Projects/CompanyTask/Escher/UnitTest/Spouse/Sush.txt";
            string filename = ConfigurationManager.AppSettings["PeoplePath"];
            RegistrationMain.StoreInfo(content, filename);
            string output;
            using (StreamReader reader = new StreamReader(filename))
            {
                output = reader.ReadLine();
            }
            Assert.AreEqual(content, output);
        }
    }
}

