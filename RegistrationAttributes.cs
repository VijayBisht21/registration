﻿using System;
using System.Linq;
using System.Globalization;
using System.IO;
namespace Registration
{
    public class RegistrationAttributes : RegistrationMain
    {

        private string _firstName;
        public string FirstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                //Validating First name is empty or not a letter
                while (!value.All(char.IsLetter) || String.IsNullOrEmpty(value))
                {
                    Console.Write("Invalid format. Please input again(Only letters) : ");
                    value = Console.ReadLine();
                }
                _firstName = value;
            }
        }
        private string _surName;
        public string SurName
        {
            get
            {
                return _surName;
            }
            set
            {
                //Validating Sur name is empty or not a letter
                while (!value.All(char.IsLetter) || String.IsNullOrEmpty(value))
                {
                    Console.Write("Invalid format. Please input again(Only letters) : ");
                    value = Console.ReadLine();
                }
                _surName = value;
            }

        }
        private string _dob;
        public string DOB
        {
            get
            {
                return _dob;
            }
            set
            {
                //Validating Date of birth is in right format
                DateTime dt;
                CultureInfo provider = CultureInfo.InvariantCulture;
                while (!DateTime.TryParseExact(value, dateformat, provider,
                              DateTimeStyles.None, out dt))
                {
                    Console.Write("Invalid format. Please input again({0}) : ", dateformat);
                    value = Console.ReadLine();
                }
                _dob = value;
            }
        }
        private string _maritalStatus;
        public string MaritalStatus
        {
            get
            {
                return _maritalStatus;
            }
            set
            {
                _maritalStatus = value;
            }
        }
        private string _spousePath;
        public string SpousePath
        {
            get
            {
                return _spousePath;
            }
            set
            {
                //Checking if the Spouse file alreay exists then create another one.
                int i = 1;
                string filePath = Path.GetDirectoryName(value);
                string newFileName = Path.GetFileNameWithoutExtension(value) + "{0}";
                while (File.Exists(value))
                {
                    value = Path.Combine(filePath, string.Format(newFileName, "(" + i++ + ")") + Path.GetExtension(value));

                }

                _spousePath = value;
            }
        }

        public string ToBuilder()
        {
            // Appending Persons information to be saved in the file.
            string res = FirstName + "|" + SurName + "|" + DOB;
            if (!String.IsNullOrEmpty(MaritalStatus))
                res += "|" + MaritalStatus + "|" + SpousePath;

            return res;

        }


    }
}
