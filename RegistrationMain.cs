﻿using System;
using System.IO;
using System.Configuration;

namespace Registration
{
    public class RegistrationMain
    {
        public static string dateformat = ConfigurationManager.AppSettings["dateformat"];
        static void Main(string[] args)
        {
            //Load welcome message from Config
            Console.WriteLine(ConfigurationManager.AppSettings["WelcomePage"]);
            Console.WriteLine("Please enter your Details");
            RegistrationAttributes reg = new RegistrationAttributes();
            // Accept Persons information
            RegistrationForm(reg);

            //Validate age for registration
            bool result = ValidateAge(reg);
            if (result)
            {
                //Check if the person is married or single
                bool checkstatus = CheckMaritalStatus(reg);
                if (checkstatus is true)
                {
                    //Copy profile information into file
                    StoreInfo(reg.ToBuilder(), ConfigurationManager.AppSettings["PeoplePath"]);
                    Console.WriteLine("Registration Completed Successfully !!");
                }
            }
            Console.ReadLine();

        }

        public static string RegistrationForm(RegistrationAttributes reg)
        {
            // Accept and Validate input from user
            Console.Write("FirstName : "); reg.FirstName = Console.ReadLine();
            Console.Write("SurName : "); reg.SurName = Console.ReadLine();
            Console.Write("Date Of Birth ({0}):", dateformat); reg.DOB = Console.ReadLine();

            //Load spouse path from config
            string spousePath = ConfigurationManager.AppSettings["SpousePath"];
            return Path.Combine(spousePath, reg.FirstName + ".txt");
        }

        public static bool ValidateAge(RegistrationAttributes age_reg)
        {
            //Convert input date of birth to specific date format
            DateTime person = DateTime.ParseExact(age_reg.DOB, dateformat, null);
            // Calculate age 
            int age = DateTime.Now.Year - person.Year;
            if (DateTime.Now.DayOfYear < person.DayOfYear)
                age -= 1;
            /* If age less than 18, ask for parents authorisation.
             * If not authorized, deny registration
             * If age less than 16, deny registration
             */
            if (age < 18)
            {
                if (age >= 16)
                {
                    Console.Write("My Parents allow registration(Y/N)?: "); string ind = Console.ReadLine();
                    if (ind.ToUpper() == "Y")
                        return true;
                    else
                    {
                        Console.Write("Registarion Denied !!");
                        return false;
                    }
                }
                else
                {
                    Console.Write("Registarion Denied !! You should be 16 and above!!");
                    return false;
                }
            }
            return true;
        }

        public static bool CheckMaritalStatus(RegistrationAttributes reg)
        {
            RegistrationAttributes spouse = new RegistrationAttributes();
            string retry;
            do
            {
                /*If married, save wife details into separate linked file
                 * If wrong input , ask them to retry
                 * If they dont want to retry then fail registration
                 */
                retry = "N";
                Console.Write("MaritalStatus : Please select one of the Options\n1- Married\n2-Single\nYour Option:");
                string option = Console.ReadLine();
                switch (option)
                {
                    case "1":
                        reg.MaritalStatus = "Married";
                        Console.WriteLine("Please enter your Spouse Details");
                        reg.SpousePath = RegistrationForm(spouse);
                        StoreInfo(spouse.ToBuilder(), reg.SpousePath);
                        break;
                    case "2": reg.MaritalStatus = "Single"; reg.SpousePath = "null"; break;
                    default: Console.WriteLine("Invalid Input. Would you like to retry (Y/N) ?"); retry = Console.ReadLine(); break;

                }
            } while (retry.ToUpper() != "N");

            if(String.IsNullOrEmpty(reg.MaritalStatus))
            {
                Console.WriteLine("Registration failed !! Please try again.");
                return false;
            }
            return true;
        }

        public static void StoreInfo(string content, string fileName)
        {
            /*Create directory if not exists else Ignore
            * Apend persons data to People.txt
            */
            try
            {
                Directory.CreateDirectory(Directory.GetParent(fileName).ToString());
                FileStream stream = new FileStream(fileName, FileMode.Append);
                using (StreamWriter writer = new StreamWriter(stream))
                {
                    writer.WriteLine(content);
                }
            }
            catch (Exception exp)
            {
                Console.Write(exp.Message);
            }
        }

    }
}
